# Void Instrument

The PureData part of Void Instrument, a interactive sound instrument based on the presence of physical objects in a scene, initiated by Raphaël Bastide, in collaboration with Alexandre Contini and Martin Meurin.

## Thanks

This project uses the work of:
- Martin Brinkmann, Randomperc 2013
- Miguel Moreno, Eucgui 2017 https://github.com/MikeMorenoAudio/mian-/tree/master/eucgui
